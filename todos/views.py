from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList
from todos.forms import ToDoListForm, ToDoItemForm
from django.http.request import HttpRequest
from django.http.response import HttpResponse
from todos.models import TodoItem

# Create your views here.


# create a view that will get all of the instances of the model
def todo_list(request):
    todo_lists = TodoList.objects.all()
    context = {"todo_lists": todo_lists}
    return render(request, "todos/list.html", context)


# create a view that shows the details of a particular to-do list, including its tasks
def list_detail(request, id):
    list_details = get_object_or_404(
        TodoList, id=id
    )  # get_object_or_404 calls the given model and gets an object from that. If the object/model does not exist it raises a 404 error
    context = {"list_details": list_details}
    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = ToDoListForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", list.id)
    else:
        form = ToDoListForm()
    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)


def todo_list_update(request: HttpRequest, id: int) -> HttpResponse:
    update = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = ToDoListForm(request.POST, instance=update)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=update.id)
    else:
        form = ToDoListForm(instance=update)
    context = {"form": form}
    return render(request, "todos/edit_list.html", context)


def todo_list_delete(request, id):
    delete_list = TodoList.objects.get(id=id)
    if request.method == "POST":
        delete_list.delete()
        return redirect("todo_list_list")
    else:
        return render(request, "todos/delete.html")


def todo_item_create(request: HttpResponse) -> HttpRequest:
    if request.method == "POST":
        form = ToDoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = ToDoItemForm()
    context = {
        "form": form,
    }
    return render(request, "todos/create_item.html", context)


def todo_item_update(request: HttpRequest, id: int) -> HttpResponse:
    update = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = ToDoItemForm(request.POST, instance=update)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=update.id)
    else:
        form = ToDoItemForm(instance=update)
    context = {"form": form}
    return render(request, "todos/edit_item.html", context)
