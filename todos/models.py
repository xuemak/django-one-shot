from django.db import models


# Create your models here.
class TodoList(models.Model):
    name = models.CharField(max_length=100)
    created_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name


class TodoItem(models.Model):
    task = models.CharField(max_length=100)
    due_date = models.DateTimeField(
        auto_now=False, null=True, blank=True
    )  # null=True allows the field to be historically empty, blank=True makes a field optional
    is_completed = models.BooleanField(default=False)
    list = models.ForeignKey(
        TodoList, related_name="items", on_delete=models.CASCADE
    )  # related_name attribute specifies the name of the reverse relation from the User model back to your model.
