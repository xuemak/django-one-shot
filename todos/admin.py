from django.contrib import admin
from todos.models import (
    TodoList,
    TodoItem,
)  # from <<app name>>.models import <<Model name>>


# Register your models here.
@admin.register(TodoList)
class TodoList(admin.ModelAdmin):
    list_display = [
        "name",
        "id",
    ]


@admin.register(TodoItem)
class TodoItem(admin.ModelAdmin):
    list_display = ["task", "due_date", "is_completed"]
